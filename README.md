[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v1.4%20adopted-ff69b4.svg)](code-of-conduct.md)
# svelte-routes

A simple router for svelte v3.

## Install

`npm i --save svelte-routes`

## Usage

In `routes.js`:

```javascript
import MyComponent from './MyComponent.svelte';
import 404Page from './404Page.svelte';

const routes = [
  {
    to: '/',
    component: MyComponent,
  },
  {
    to: '*',
    component: 404Page,
  },
];

export default routes;
```

In `App.svelte`:

```html
<script>
  import routes from './routes';
  import { Router } from 'svelte-routes';
</script>

<Router routes={routes} />
```

## Code of Conduct

Please note that this project is released with a [Contributor Code of Conduct](CODE_OF_CONDUCT.md)).
By participating in this project you agree to abide by its terms.
